import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";
import Pdf from "./Pdf";
import { PDFViewer } from "@react-pdf/renderer";

function App() {
  const [data, setData] = useState({});

  useEffect(() => {
    async function getApi() {
      const urlApi = "https://indonesia-covid-19.mathdro.id/api/provinsi/";
      const { data: listProvinsi } = await axios.get(urlApi);

      const dataDki = listProvinsi.data.find((provinsi) => provinsi.fid === 11);
      console.log("Api data:", dataDki);
      setData(dataDki);
    }
    getApi();
  }, []);

  return (
    <div className="App">
      <PDFViewer height="500px" style={{ width: `400px` }}>
        <Pdf data={data} />
      </PDFViewer>
    </div>
  );
}

export default App;
