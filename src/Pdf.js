import React from "react";
import { Document, Page, View, Text } from "@react-pdf/renderer";

const Pdf = ({ data }) => {
  return (
    <Document>
      <Page size="A4">
        <View>
          <Text>{data.provinsi}</Text>
          <Text>{data.kasusMeni}</Text>
          <Text>{data.kasusPosi}</Text>
          <Text>{data.kasusSemb}</Text>
        </View>
      </Page>
    </Document>
  );
};

export default Pdf;
