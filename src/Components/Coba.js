import React, { useState } from "react";
import { Document, Page } from "react-pdf";

const options = {
  cMapUrl: "cmaps/",
  cMapPacked: true,
};

function Coba() {
  const [file, setFile] = useState("./");
  const [numPages, setNumPages] = useState(null);

  const onFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const onDocumentLoadSuccess = ({ numPages: nextNumPages }) => {
    setNumPages(nextNumPages);
  };

  //   return (
  //     <div className="App">
  //       <Document
  //         file={file}
  //         onLoadSuccess={onDocumentLoadSuccess}
  //         options={options}
  //       >
  //         {Array.from(new Array(numPages), (el, index) => (
  //           <Page></Page>
  //         ))}
  //       </Document>
  //     </div>
  //   );
}

export default Coba;
